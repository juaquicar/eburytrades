# README #

Debido al déficit de tiempo del que dispongo actualmente por mi actual trabajo (al cual dedico bastantes horas extras), 
ha sido imposible completar el 100% de la prueba en los plazos establecidos, sin embargo se ha optado por soluciones 
más rápidas de implementar para que sea 100% funcional. Apenas he podido dedicar a la prueba un rato el pasado
Domingo y otro rato hoy.

Tareas incompletas: El formulario de envío de Trade se ha implementado utilizando los tradicionales FORM de 
DJANGO en vez de POST hacia la API-REST Los Scripts de instalación: Script de puesta en producción sobre 
APACHE2+WGSI no realizado. Script de puesta de desarrollo: me he limitado a poner los comandos, sin 
programar mucho bash. No he implementado las medidas de seguridad que hubiese deseado.

Saludos,

Juan Manuel Quijada quijada.jm@gmail.com https://www.linkedin.com/in/juanmaquijada/ https://www.ispfacil.es/
