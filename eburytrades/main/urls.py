from .views import index, new_trade, TRADEViewset
from django.conf.urls import include, url
from django.urls import path
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'trade', TRADEViewset)


urlpatterns = [
    path(r'', index, name='index'),
    path(r'new_trade/', new_trade, name='new_trade'),
    url(r'^', include(router.urls)),
]
