from django.db import models
from django.utils import timezone
# Create your models here.



class Trade(models.Model):
    id =  models.CharField(max_length=9, unique=True, primary_key=True)
    sell_currency = models.CharField(max_length=3)
    sell_amount = models.FloatField(default=None)
    buy_currency = models.CharField(max_length=3)
    buy_amount = models.FloatField(default=None)
    rate =  models.FloatField(default=None)
    date_booked = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '%s - ( %s %s is %s %s)' % (self.id, self.sell_amount, self.sell_currency, self.buy_amount, self.buy_currency )
