from django import forms

class TradeForm(forms.Form):
    sell_currency = forms.CharField(max_length=3, required=True)
    buy_currency = forms.CharField(max_length=3, required=True)
    sell_amount = forms.FloatField(required=True)
    buy_amount = forms.FloatField(required=True)
    rate = forms.FloatField(required=True)
