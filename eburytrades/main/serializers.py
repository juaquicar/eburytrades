from rest_framework.serializers import ModelSerializer
from .models import Trade

class TradeSerializer(ModelSerializer):
    class Meta:
        model = Trade
        fields = ('id', 'sell_currency', 'sell_amount', 'buy_currency', 'buy_amount', 'rate', 'date_booked')
        read_only_fields = ('id', 'date_booked')
