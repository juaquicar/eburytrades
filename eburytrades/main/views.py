from django.shortcuts import render
from django.utils import timezone
from django.shortcuts import redirect
import random
import json
from urllib3 import PoolManager
from django.conf import settings
from .models import Trade
from .forms import TradeForm
from main.serializers import TradeSerializer
from rest_framework import viewsets


def index(request):
    return render(request, 'index.html')


class TRADEViewset(viewsets.ModelViewSet):
    serializer_class = TradeSerializer
    queryset = Trade.objects.all().order_by('-date_booked')
    lookup_field = 'id'


def new_trade(request):
    list_symbol = get_symbols()
    if request.method == 'POST':
        form = TradeForm(request.POST)
        if form.is_valid():
            new_id = 'TR' + random_generator(7)
            trade = Trade(id = new_id,
                            sell_currency = form.cleaned_data['sell_currency'],
                            sell_amount = form.cleaned_data['sell_amount'],
                            buy_currency = form.cleaned_data['buy_currency'],
                            buy_amount = form.cleaned_data['buy_amount'],
                            rate = form.cleaned_data['rate'],
                            date_booked = timezone.now(),
                          )
            trade.save()
            return redirect('index')
    else:
        form = TradeForm()
    return render(request, 'new_trade.html', { 'form': form, 'list_symbol': list_symbol, 'fixer_key': settings.FIXER_KEY})

def random_generator(size):
    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return ''.join(random.choice(chars) for x in range(size))

def get_symbols():
    key = settings.FIXER_KEY
    base = 'http://data.fixer.io/api/symbols?access_key='
    url = base + key
    manager = PoolManager(1)
    response = manager.request('GET', url)
    data = json.loads(response.data)
    list_symbol = []
    for item in data['symbols']:
        list_symbol.append(str(item))
    return list_symbol

