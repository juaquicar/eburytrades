#!/bin/bash

apt-get install python3-pip
apt-get install virtualenv
cd eburytrades
source bin/activate
cd eburytrades
pip3 install -r requirements-dev.txt
python3 manage.py runserver 0.0.0.0:8000